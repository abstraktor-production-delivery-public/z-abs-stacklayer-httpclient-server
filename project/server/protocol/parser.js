
'use strict';

const HttpConst = require('../const/http-const');
const HttpMsgRequest = require('../msg/http-msg-request');
const HttpMsgResponse = require('../msg/http-msg-response');
const BufferManager = require('z-abs-stacklayer-core-server/connection/buffer-manager');


class Parser {
  constructor(id, type, replaceHeaderNames = new Map()) {
    this.id = id;
    this.currentId = ++Parser.CURRENT_ID;
    this.type = type;
    this.replaceHeaderNames = replaceHeaderNames;
    this.bufferManager = new BufferManager(this.id, true);
    this.state = Parser.NONE;
    this.size = 0;
    this.sizeLeft = 0;
    this.chunks = null;
    this.message = null;
    this.rawBody = [];
  }
  
  parse(chunk) {
    this.bufferManager.receiveBuffer(chunk);
    switch(this.state) {
      case Parser.NONE: {
        if(!this.bufferManager.findLine()) {
          return Parser.RESULT_NONE;
        }
        else {
          this.size = 0;
          this.sizeLeft = 0;
          this.rawBody = [];
          this.chunks = null;
          const line = this.bufferManager.getLine();
          if(Parser.REQUEST === this.type) {
            this.message = new HttpMsgRequest();
            this.message.addRequestLine(...line.split(' '));
          }
          else {
            this.message = new HttpMsgResponse();
            this.message.addStatusLine(...line.split(' '));
          }
          this._setState(Parser.HEADERS);
        }
      }
      case Parser.HEADERS: {
        let allHeaders = false;
        while(this.bufferManager.findLine()) {
          const line = this.bufferManager.getLine();
          if(0 === line.length) {
            this._setState(Parser.BODY);
            allHeaders = true;
            break;
          }
          const delimiter = line.indexOf(':');
          if(-1 === delimiter) {
            console.log('parse Error:', delimiter);
          }
          this.message.addHeader(line.substring(0, delimiter), line.substring(delimiter + 1).trimStart());
        }
        if(!allHeaders) {
          return Parser.RESULT_NONE;
        }
      }
      case Parser.BODY: {
        const size = this.message.getHeader('Content-Length');
        if(size) {
          this.size = this.sizeLeft = Number.parseInt(size);
          this._setState(Parser.BODY_CONTENT_LENGTH);
        }
        else {
          const transferEncoding = this.message.getHeader('Transfer-Encoding');
          if(transferEncoding) {
            this._setState(Parser.BODY_TRANSFER_ENCODING);
            return this._parseTransferEncoding(chunk);
          }
          else {
            this._setState(Parser.NONE);
            return Parser.RESULT_BODY_ALL;
          }
        }
      }
      case Parser.BODY_CONTENT_LENGTH: {
        if(0 === this.size) {
          this._setState(Parser.NONE);
          return Parser.RESULT_BODY_ALL;
        }
        const buffer = this.bufferManager.receiveSize(this.size);
        if(undefined !== buffer) {
          this.sizeLeft -= buffer.length;
          this.message.addBody(buffer);
          this.rawBody.push(buffer);
          if(0 === this.sizeLeft) {
            this._setState(Parser.NONE);
            return Parser.RESULT_BODY_ALL;
          }
        }
        break;
      }
      case Parser.BODY_TRANSFER_ENCODING: {
        return this._parseTransferEncoding(chunk);
      }
      default: {
        
      }
    }
    return false;
  }
  
  _parseTransferEncoding(chunk) {
    if(null === this.chunks) {
      this.chunks = {
        state: Parser.TE_SIZE,
        size: 0,
        chunkLines: [],
        chunks: [],
        trailers: []
      };
    }
    
    switch(this.chunks.state) {
      case Parser.TE_SIZE:
      case Parser.TE_DATA:
      case Parser.TE_DATA_LINE: {
        // 1) Get Data
        while(true) {
          if(Parser.TE_SIZE === this.chunks.state) {
            if(!this.bufferManager.findLine()) {
              return Parser.RESULT_HEADERS;
            }
            this.chunks.state = Parser.TE_DATA;
            const line = this.bufferManager.getLine();
            this.chunks.chunkLines.push(line);
            const chunkLineIndex = line.indexOf(HttpConst.COMMA_SP);
            if(-1 === chunkLineIndex) {
              this.chunks.size = Number.parseInt(line, 16);
            }
            else {
              this.chunks.size = Number.parseInt(line.substring(0, chunkLineIndex), 16);
            }
          }
          if(Parser.TE_DATA === this.chunks.state) {
            if(0 !== this.chunks.size) {
              if(Number.isNaN(this.chunks.size)) {
                return true; // THROW ?
              }
              const buffer = this.bufferManager.receiveSize(this.chunks.size);
              if(undefined !== buffer) {
                this.message.addBody(buffer);
                this.chunks.chunks.push(buffer);
                this.chunks.size = 0;
                this.chunks.state = Parser.TE_DATA_LINE;
              }
              else {
                return Parser.RESULT_HEADERS;
              }
            }
            else {
              this.chunks.state = Parser.TE_TRAILER;
              break;
            }
          }
          if(Parser.TE_DATA_LINE === this.chunks.state) {
            if(!this.bufferManager.findLine()) {
              return Parser.RESULT_HEADERS;
            }
            const line = this.bufferManager.getLine();
            this.chunks.state = Parser.TE_SIZE;
          }
        }
        break;
      }
    };
    switch(this.chunks.state) {
      case Parser.TE_TRAILER: {
        // 2) Get Trailers
        while(true) {
          if(!this.bufferManager.findLine()) {
            return Parser.RESULT_HEADERS;
          }
          const line = this.bufferManager.getLine();
          if(0 === line.length) {
            this.chunks.state = Parser.TE_DONE;
            break;
          }
          this.chunks.trailers.push(line);
        }
      }
    };
    this._setState(Parser.NONE);
    return true;
  }
  
  isHeadersReady() {
    return this.state > Parser.HEADERS;
  }
  
  _setState(state) {
    this.state = state;
  }
}

Parser.RESULT_NONE = 0;
Parser.RESULT_HEADERS = 1;
Parser.RESULT_BODY_PART = 2;
Parser.RESULT_BODY_ALL = 2;

Parser.REQUEST = 0;
Parser.RESPONSE = 1;

Parser.NONE = 0;
Parser.HEADERS = 1;
Parser.BODY = 2;
Parser.BODY_CONTENT_LENGTH = 3;
Parser.BODY_TRANSFER_ENCODING = 4;

Parser.STATUS = [
  'NONE',
  'HEADERS',
  'BODY',
  'BODY_CONTENT_LENGTH',
  'BODY_TRANSFER_ENCODING'
];

Parser.TE_SIZE = 0;
Parser.TE_DATA = 1;
Parser.TE_DATA_LINE = 2;
Parser.TE_TRAILER = 3;
Parser.TE_DONE = 4;

Parser.TE_STATUS = [
  'TE_SIZE',
  'TE_DATA',
  'TE_DATA_LINE',
  'TE_TRAILER',
  'TE_DONE'
];

Parser.CURRENT_ID = 0;


module.exports = Parser;

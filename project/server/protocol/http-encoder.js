
'use strict';


class HttpEncoder {
  static encode(msg) {
    const size = msg.method.length + msg.requestTarget.length + msg.httpVersion.length + 4 + HttpEncoder.getHeadersLength(msg);
    const buffer = Buffer.allocUnsafe(size);
    let offset = 0;
    offset += buffer.write(`${msg.method} ${msg.requestTarget} ${msg.httpVersion}\r\n`, offset);
    msg.headers.forEach((values, name) => {
      values.forEach((value) => {
        offset += buffer.write(name, offset);
        offset += buffer.write(': ', offset);
        offset += buffer.write(value, offset);
        offset += buffer.write('\r\n', offset);
      });
    });
    offset += buffer.write('\r\n', offset);
    return buffer;
  }
  
  static getHeadersLength(msg) {
    let length = 0;
    msg.headers.forEach((values, name) => {
      values.forEach((value) => {
        length += name.length + value.length + 4;
      });
    });
    length += 2;
    return length;
  }
}

module.exports = HttpEncoder;

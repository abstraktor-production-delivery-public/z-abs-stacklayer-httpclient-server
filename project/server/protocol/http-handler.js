
'use strict';

const HttpGet = require('../msg/http-get');
const HttpEncoder = require('./http-encoder');
const HttpParser = require('./parser');


class HttpHandler {
  constructor(tcpClient, host) {
    this.tcpClient = tcpClient;
    this.host = host;
    this.parser = new HttpParser(++HttpHandler.id, HttpParser.RESPONSE);
  }
  
  send(requestTarget, cb, cbConnection) {
    const httpRequest = new HttpGet(requestTarget, 'www.actorjs.com', 'image/*');
    this.tcpClient.send(HttpEncoder.encode(httpRequest), (data) => {
      if(this.parser.parse(data)) {
        const httpResponse = this.parser.message;
        const statusCode = httpResponse.statusCode;
        cb(statusCode, httpResponse, httpRequest);
        return true;
      }
      else {
        return false;
      }
    }, () => {}, cbConnection);
  }
}

HttpHandler.id = 0;

module.exports = HttpHandler;


'use strict';

const HttpHandler = require('./http-handler');
const TcpClient = require('z-abs-stacklayer-core-server/connection/tcp-client');


class HttpConnection {
  constructor(name) {
    this.name = name;
    this.tcpClient = null;
    this.httpHandler = null;
  }
  
  send(requestTarget, cb, cbConnection) {
      this.httpHandler.send(requestTarget, cb, cbConnection);
  }
  
  connect(port) {
    this.tcpClient = new TcpClient((connected, downTicks) => {
      this.httpHandler = new HttpHandler(this.tcpClient, 'www.actorjs.com');
   //   console.log(`Connecting[http]: ${this.name}:`, connected, 'Tick:', downTicks);
    }, (err) => {
      if('ECONNREFUSED' !== err.code) {
        console.log(`Connecting[http]: ${this.name} - Error:`, err);
      }
    });
    this.tcpClient.start('127.0.0.1', 0, '127.0.0.1', port);
  }
  
  disconnect(connection, done) {
    this.tcpClient.stop(() => {
      this.httpHandler = null;
      this.tcpClient = null;
      done();
    });
  }
}


module.exports = HttpConnection;


'use strict';

const HttpMsg = require('./http-msg');


class HttpMsgRequest extends HttpMsg {
  constructor(method, requestTarget, httpVersion) {
    super();
    this.method = method;
    this.requestTarget = requestTarget;
    this.httpVersion = httpVersion;
  }
 
  addRequestLine(method, requestTarget, httpVersion) {
    this.method = method;
    this.requestTarget = requestTarget;
    this.httpVersion = httpVersion;
  }
}

module.exports = HttpMsgRequest;

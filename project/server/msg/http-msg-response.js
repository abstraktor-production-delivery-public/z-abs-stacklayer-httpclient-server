
'use strict';

const HttpMsg = require('./http-msg');


class HttpMsgResponse extends HttpMsg {
  constructor(httpVersion, statusCode, reasonPhrase) {
    super();
    this.httpVersion = httpVersion;
    this.statusCode = 0
    this.statusCodeString = '';
    this.reasonPhrase = reasonPhrase;
    this._updateStatusCode(statusCode);
  }

  addStatusLine(httpVersion, statusCode, reasonPhrase) {
    this.httpVersion = httpVersion;
    this._updateStatusCode(statusCode);
    this.reasonPhrase = reasonPhrase;
  }
  
  _updateStatusCode(statusCode) {
    if(typeof statusCode === "string") {
      this.statusCode = Number.parseInt(statusCode);
      this.statusCodeString = statusCode;
    }
    else if(typeof statusCode === "number") {
      this.statusCode = statusCode;
      this.statusCodeString = "" + statusCode;
    }
  }
}

module.exports = HttpMsgResponse;


'use strict';

const HttpMsgRequest = require('./http-msg-request');


class HttpGet extends HttpMsgRequest {
  constructor(requestUri, host, accept) {
    super('GET', requestUri, 'HTTP/1.1');
    this.addHeader('Host', host);
    this.addHeader('Connection', 'keep-alive');
    this.addHeader('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/92.0.4512.0 Safari/537.36');
    this.addHeader('Accept-Encoding', '');
    this.addHeader('Accept-Language', 'en-GB');
    this.addHeader('Accept', accept);
  }
}

module.exports = HttpGet;


'use strict';


class HttpMsg {
  constructor() {
    this.headers = new Map();
    this.body = [];
  }
  
  addHeader(name, value) {
    if(undefined !== value) {
      let convertedValue = value;
      if(typeof value === 'string') {}
      else if(typeof value === 'number') {
        convertedValue = '' + value;
      }
      else if(typeof value === 'boolean') {
        convertedValue = value ? 'true' : 'false';
      }
      else {
        throw new Error('Not Allowed Header value type');
      }
      if(!this.headers.has(name)) {
        this.headers.set(name, []);
      }
      this.headers.get(name).push(convertedValue);
    }
  }
  
  getHeader(name) {
    const headers = this.headers.get(name);
    if(undefined !== headers) {
      return headers[0];
    }
  }
  
  hasBody() {
    return 0 !== this.body.length;
  }
  
  addBody(data) {
    this.body.push(data);
  }
  
  getBody() {
    if(0 === this.body.length) {
      return null;
    }
    else if(1 === this.body.length) {
      return this.body[0];
    }
    else {
      return Buffer.concat(this.body);
    }
  }
}

module.exports = HttpMsg;
